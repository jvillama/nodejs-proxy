var proxyServer = require('./proxy.js');

var config = {
  addProxyHeader: false,
  portRewrite: {
    19080: 80
  }
};

var httpProxyServer = proxyServer.createServer(config);
httpProxyServer.listen(parseInt(process.argv[2]))
